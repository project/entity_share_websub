<?php

namespace Drupal\entity_share_websub_subscriber\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event for content synchronization.
 */
class ContentSyncEvent extends Event {

  /**
   * Subscription events.
   */
  const EVENT_SYNC = 'entity_share_websub_subscriber.sync_subscription';

  /**
   * The subscription object.
   *
   * @var object
   */
  protected $subscription;

  /**
   * Constructs ContentSyncEvent.
   *
   * @param object $subscription
   *   Subscription data retrieved from storage.
   */
  public function __construct(object $subscription) {
    $this->subscription = $subscription;
  }

  /**
   * Gets the subscription from the event.
   *
   * @return object
   *   Subscription data retrieved from the event.
   */
  public function getSubscription() {
    return $this->subscription;
  }

}
