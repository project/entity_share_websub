<?php

namespace Drupal\entity_share_websub_subscriber;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use GuzzleHttp\Client;

/**
 * Subscription communications with the Hub.
 */
class Subscriber {
  /**
   * Verification Modes.
   */
  const VERIFICATION_MODE_SYNC = 'sync';
  const VERIFICATION_MODE_ASYNC = 'async';

  /**
   * Subscription Modes.
   */
  const MODE_SUBSCRIBE = 'subscribe';
  const MODE_UNSUBSCRIBE = 'unsubscribe';

  /**
   * Subscription States.
   */
  const SUBSCRIPTION_NOT_VERIFIED = 2;
  const SUBSCRIPTION_VERIFIED = 1;
  const SUBSCRIPTION_CANCELLED = 0;

  /**
   * The prefix for creating secret.
   */
  const SECRET_PREFIX = 'subscription_';

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * THe remote identifier.
   *
   * @var int
   */
  protected $remoteId;

  /**
   * The channel identifier.
   *
   * @var int
   */
  protected $channelId;

  /**
   * The entity UUID.
   *
   * @var string
   */
  protected $uuid;

  /**
   * The subscription key.
   *
   * @var string
   */
  protected $subscriptionKey;

  /**
   * The email of the user.
   *
   * @var string
   */
  protected $email;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $config;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Construct a repository object.
   *
   * @param \GuzzleHttp\Client $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Config\ConfigFactory $config
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(Client $http_client, ConfigFactory $config, EntityTypeManagerInterface $entity_type_manager) {
    $this->httpClient = $http_client;
    $this->config = $config;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Send subscribe request to hub.
   *
   * @return bool
   *   The subscription result.
   */
  public function subscribe() {
    $response = $this->sendRequest(static::MODE_SUBSCRIBE);

    if ($response->getStatusCode() == 202) {
      return TRUE;
    }

    // @todo Fix problem of not handling/logging errors here.
    return FALSE;
  }

  /**
   * Send unsubscribe request to hub.
   *
   * @return bool
   *   The unsubscribe result.
   */
  public function unsubscribe() {
    $response = $this->sendRequest(static::MODE_UNSUBSCRIBE);

    if ($response->getStatusCode() == 202) {
      return TRUE;
    }

    // @todo Fix problem of not handling/logging errors here.
    return FALSE;
  }

  /**
   * Send subscribe or unsubscribe request to hub.
   *
   * @param string $mode
   *   Subscribe or unsubscribe mode.
   */
  protected function sendRequest($mode) {
    $endpoint = $this->getHubEndpoint();
    $secret = static::getSecret($this->getRemoteId(), $this->getChannelId(), $this->getUuid());
    return $this->httpClient->request('POST', $endpoint, [
      'form_params' => [
        'hub_topic' => $this->getChannelId() . '/' . $this->getUuid(),
        'hub_mode' => $mode,
        'hub_callback' => $this->getCallbackUrl(),
        'hub_secret' => $secret,
        'hub_verify' => static::VERIFICATION_MODE_SYNC,
        'email' => $this->getEmail(),
      ],
    ]);
  }

  /**
   * Compile subscription's secret based on entity UUID.
   *
   * @param string $remote_id
   *   The remote identifier.
   * @param string $channel_id
   *   THe channel identifier.
   * @param string $uuid
   *   Entity's UUID.
   *
   * @return string
   *   The secret key for subscription to this entity.
   */
  public static function getSecret($remote_id, $channel_id, $uuid) {
    $data = implode('_', [$remote_id, $channel_id, $uuid]);
    return Crypt::hmacBase64($data, $data . Settings::getHashSalt());
  }

  /**
   * Generate subscription key which used into the callback URL.
   *
   * @return string
   *   The random hash.
   */
  public static function generateSubscriptionKey() {
    $random = Crypt::randomBytesBase64(55);
    return Crypt::hmacBase64($random, $random . Settings::getHashSalt());
  }

  /**
   * Get the hub URL from remote info.
   *
   * @return string
   *   The hub URL.
   */
  protected function getHubEndpoint() {
    $remote = $this->entityTypeManager->getStorage('remote')->load($this->getRemoteId());
    $config = $this->config->get('entity_share_websub_subscriber.settings');

    if (empty($remote)) {
      throw new \Exception('Unknown remote.');
    }

    if ($config->get('subscribe_hub_url') != '') {
      return $remote->get('url') . $config->get('subscribe_hub_url');
    }
    else {
      return $remote->get('url') . '/subscribe';
    }
  }

  /**
   * Get the callback URL for provided secret key.
   *
   * @return string
   *   The absolute URL to callback.
   */
  protected function getCallbackUrl() {
    return Url::fromRoute('entity_share_websub_subscriber.verify', [
      'subscription_key' => $this->getSubscriptionKey(),
    ], ['absolute' => TRUE])->toString();
  }

  /**
   * Set remote identifier.
   *
   * @param int $remote_id
   *   Remote identifier.
   *
   * @return $this
   *   Fluent interface.
   */
  public function setRemoteId($remote_id) {
    $this->remoteId = $remote_id;
    return $this;
  }

  /**
   * Get remote identifier.
   *
   * @return int
   *   The remote identifier.
   */
  public function getRemoteId() {
    return $this->remoteId;
  }

  /**
   * Set channel identifier.
   *
   * @param int $channel_id
   *   Channel identifier.
   *
   * @return $this
   *   Fluent interface.
   */
  public function setChannelId($channel_id) {
    $this->channelId = $channel_id;
    return $this;
  }

  /**
   * Get channel identifier.
   *
   * @return int
   *   The channel identifier.
   */
  public function getChannelId() {
    return $this->channelId;
  }

  /**
   * Set channel identifier.
   *
   * @param string $uuid
   *   The UUID of the entity.
   *
   * @return $this
   *   Fluent interface.
   */
  public function setUuid($uuid) {
    $this->uuid = $uuid;
    return $this;
  }

  /**
   * Get UUID of the entity.
   *
   * @return string
   *   The UUID of the entity.
   */
  public function getUuid() {
    return $this->uuid;
  }

  /**
   * Set subscription key.
   *
   * @param string $key
   *   The subscription kry.
   *
   * @return $this
   *   Fluent interface.
   */
  public function setSubscriptionKey($key) {
    $this->subscriptionKey = $key;
    return $this;
  }

  /**
   * Get subscription key.
   *
   * @return string
   *   The subscription key.
   */
  public function getSubscriptionKey() {
    return $this->subscriptionKey;
  }

  /**
   * Set email of the user.
   *
   * @param string $email
   *   Email of the user.
   *
   * @return $this
   *   Fluent interface.
   */
  public function setEmail($email) {
    $this->email = $email;
    return $this;
  }

  /**
   * Get email of the user.
   *
   * @return string
   *   The email related to subscription.
   */
  public function getEmail() {
    return $this->email ?: $this->config->get('system.site')->get('mail');
  }

}
