<?php

namespace Drupal\entity_share_websub_subscriber;

use Drupal\entity_share_websub_subscriber\Event\ContentSyncEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Maintain local subscription data.
 */
class SubscriptionManager {

  /**
   * The Subscription repository.
   *
   * @var \Drupal\entity_share_websub_subscriber\SubscriptionRepository
   */
  protected $repository;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public function __construct(SubscriptionRepository $subscriptionRepository, EventDispatcherInterface $event_dispatcher) {
    $this->repository = $subscriptionRepository;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * Process subscribe request and return subscription status.
   *
   * @param string $remote_id
   *   The remote identifier.
   * @param string $channel_id
   *   The channel identifier.
   * @param string $uuid
   *   The UUID of the entity.
   *
   * @return bool
   *   TRUE if hub needs to be notified, FALSE otherwise.
   */
  public function subscribe($remote_id, $channel_id, $uuid) {
    $subscription = $this->getSubscription($remote_id, $channel_id, $uuid);

    // Create new subscription, with not verified status.
    if (empty($subscription)) {
      $this->createSubscription($remote_id, $channel_id, $uuid);
      return TRUE;
    }

    if ($subscription->status == Subscriber::SUBSCRIPTION_NOT_VERIFIED) {
      return TRUE;
    }

    // Re-subscribe - hub verification is not needed, but we need to notify hub
    // about resume of the subscription.
    if (in_array($subscription->status, [
      Subscriber::SUBSCRIPTION_CANCELLED,
    ])) {
      $this->updateSubscriptionStatus($subscription->id, Subscriber::SUBSCRIPTION_VERIFIED);
      // And we need to update the local copy.
      $this->eventDispatcher->dispatch(new ContentSyncEvent($subscription), ContentSyncEvent::EVENT_SYNC);
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Process unsubscribe request and return subscription status.
   *
   * @param string $remote_id
   *   The remote identifier.
   * @param string $channel_id
   *   The channel identifier.
   * @param string $uuid
   *   The UUID of the entity.
   *
   * @return bool
   *   TRUE if hub needs to be notified, FALSE otherwise.
   */
  public function unsubscribe($remote_id, $channel_id, $uuid) {
    $subscription = $this->getSubscription($remote_id, $channel_id, $uuid);

    // Attempt to unsubscribe of non existed subscription or
    // already closed subscription.
    if (empty($subscription) || $subscription->status == Subscriber::SUBSCRIPTION_CANCELLED) {
      return FALSE;
    }

    // Cancelled status is used for records which were approved by hub only.
    // In case when subscription wasn't verified we can't set cancelled status
    // and need to remove record.
    if ($subscription->status == Subscriber::SUBSCRIPTION_NOT_VERIFIED) {
      $this->deleteSubscription($subscription->id);
      return FALSE;
    }

    $this->updateSubscriptionStatus($subscription->id, Subscriber::SUBSCRIPTION_CANCELLED);
    return TRUE;
  }

  /**
   * Load existed subscription.
   *
   * @param string $remote_id
   *   The remote identifier.
   * @param string $channel_id
   *   The channel identifier.
   * @param string $uuid
   *   The UUID of the entity.
   *
   * @return object|false
   *   The subscription object.
   */
  public function getSubscription($remote_id, $channel_id, $uuid) {
    return $this->repository->load([
      'remote_id' => $remote_id,
      'channel_id' => $channel_id,
      'uuid' => $uuid,
    ]);
  }

  /**
   * Create subscription.
   *
   * @param string $remote_id
   *   The remote identifier.
   * @param string $channel_id
   *   The channel identifier.
   * @param string $uuid
   *   The UUID of the entity.
   *
   * @return int
   *   The count of created entities.
   */
  protected function createSubscription($remote_id, $channel_id, $uuid) {
    return $this->repository->insert([
      'remote_id' => $remote_id,
      'channel_id' => $channel_id,
      'uuid' => $uuid,
      'subscription_key' => Subscriber::generateSubscriptionKey(),
      'secret' => Subscriber::getSecret($remote_id, $channel_id, $uuid),
      'status' => Subscriber::SUBSCRIPTION_NOT_VERIFIED,
    ]);
  }

  /**
   * Update status of subscription.
   *
   * @param int $id
   *   The subscription identifier.
   * @param int $status
   *   New status of subscription.
   *
   * @return int
   *   The count of created entities.
   */
  protected function updateSubscriptionStatus($id, $status) {
    return $this->repository->update([
      'id' => $id,
      'status' => $status,
    ]);
  }

  /**
   * Delete the subscription.
   *
   * @param int $id
   *   The subscription identifier.
   *
   * @return int
   *   The count of created entities.
   */
  protected function deleteSubscription($id) {
    return $this->repository->delete($id);
  }

  /**
   * Get the label of the status.
   *
   * @param int $status
   *   The status.
   *
   * @return string
   *   The label.
   */
  public static function getStatusLabel($status) {

    $map = [
      Subscriber::SUBSCRIPTION_CANCELLED => 'Cancelled',
      Subscriber::SUBSCRIPTION_VERIFIED => 'Subscribed',
      Subscriber::SUBSCRIPTION_NOT_VERIFIED => 'Not verified',
    ];

    return $map[$status] ?? '';
  }

}
