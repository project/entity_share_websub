<?php

namespace Drupal\entity_share_websub_subscriber\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Queue\QueueFactory;
use Drupal\entity_share_websub\SignatureTrait;
use Drupal\entity_share_websub_subscriber\Event\ContentSyncEvent;
use Drupal\entity_share_websub_subscriber\Subscriber;
use Drupal\entity_share_websub_subscriber\SubscriptionRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Respond to requests related to subscriptions.
 *
 * @phpstan-consistent-constructor
 */
class SubscriptionController extends ControllerBase {

  use SignatureTrait;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    protected SubscriptionRepository $subscriptionRepository,
    protected QueueFactory $queue,
    protected EventDispatcherInterface $eventDispatcher,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_share_websub_subscriber.subscription_repository'),
      $container->get('queue'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * Verify subscription.
   *
   * @param string $subscription_key
   *   The subscription key.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   */
  public function handleVerification($subscription_key, Request $request) {
    $subscription = $this->subscriptionRepository->loadBySubscriptionKey($subscription_key);

    if ($subscription && $this->isValidHubVerification($request, $subscription->secret)) {

      // Check that hub means the same topic.
      if ($subscription->uuid != $request->get('hub_topic')) {

        // If subscription is not verified then mark it as verified and
        // process initial entity import asynchronously.
        if ($subscription->status == Subscriber::SUBSCRIPTION_NOT_VERIFIED) {
          $this->subscriptionRepository->update([
            'id' => $subscription->id,
            'status' => Subscriber::SUBSCRIPTION_VERIFIED,
          ]);

          // Import selected entity after verification.
          // @see Drupal\entity_share_websub_subscriber\SubscriptionRecordWorker
          $this->eventDispatcher->dispatch(new ContentSyncEvent($subscription), ContentSyncEvent::EVENT_SYNC);
          $result = Xss::filter($request->get('hub_challenge'));
          return new Response($result, Response::HTTP_OK);
        }
      }
    }
    return new Response(NULL, Response::HTTP_NOT_FOUND);
  }

  /**
   * Handle update notification.
   *
   * @param string $subscription_key
   *   The subscription key.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   */
  public function handleUpdateNotification($subscription_key, Request $request) {
    $subscription = $this->subscriptionRepository->loadBySubscriptionKey($subscription_key);

    if ($subscription) {
      $signature = $this->getSignature($request->getContent(), $subscription->secret);

      // Validate signature.
      if ($signature == $request->headers->get('X-Hub-Signature')) {
        $this->eventDispatcher->dispatch(new ContentSyncEvent($subscription), ContentSyncEvent::EVENT_SYNC);
      }
    }

    // Return standard response even in case of broken validation
    // to prevent brute-forcing.
    return new Response('Update confirmed', Response::HTTP_ACCEPTED);
  }

  /**
   * Handle delete notification.
   *
   * @param string $subscription_key
   *   The subscription key.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   */
  public function handleDeleteNotification($subscription_key, Request $request) {
    $subscription = $this->subscriptionRepository->loadBySubscriptionKey($subscription_key);

    if ($subscription) {
      $signature = $this->getSignature($request->query->get('topic'), $subscription->secret);

      // Validate signature.
      if ($signature == $request->headers->get('X-Hub-Signature')) {
        $this->subscriptionRepository->update([
          'id' => $subscription->id,
          'status' => Subscriber::SUBSCRIPTION_CANCELLED,
        ]);
      }
    }

    // Return standard response even in case of broken validation
    // to prevent brute-forcing.
    return new Response('Delete confirmed', Response::HTTP_ACCEPTED);
  }

  /**
   * Validation of the verification request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   * @param string $secret
   *   The secret of the subscription.
   *
   * @return bool
   *   Result of the validation.
   */
  protected function isValidHubVerification(Request $request, $secret) {

    $mode = $request->get('hub_mode');

    if (empty($mode) || !in_array($mode, [
      Subscriber::MODE_SUBSCRIBE,
      Subscriber::MODE_UNSUBSCRIBE,
    ])) {
      return FALSE;
    }

    $topic = $request->get('hub_topic');

    if (empty($topic)) {
      return FALSE;
    }

    $challenge = $request->get('hub_challenge');

    if (empty($challenge)) {
      return FALSE;
    }

    if ($mode == Subscriber::MODE_SUBSCRIBE) {
      $lease_seconds = $request->get('hub_lease_seconds');
      if (empty($lease_seconds)) {
        return FALSE;
      }
    }

    // Validate hub signature.
    $signature = $this->getSignature([
      'hub.mode' => $request->query->get('hub_mode'),
      'hub.topic' => $request->query->get('hub_topic'),
      'hub.challenge' => $request->query->get('hub_challenge'),
      'hub.lease_seconds' => $request->query->get('hub_lease_seconds'),
    ], $secret);

    if ($signature != $request->headers->get('X-Hub-Signature')) {
      return FALSE;
    }

    return TRUE;
  }

}
