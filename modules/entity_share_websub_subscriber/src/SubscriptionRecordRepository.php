<?php

namespace Drupal\entity_share_websub_subscriber;

use Drupal\Core\Database\Connection;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Storage service for subscription records.
 */
class SubscriptionRecordRepository {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Construct a repository object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translation
   *   The translation service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(Connection $connection, TranslationInterface $translation, MessengerInterface $messenger) {
    $this->connection = $connection;
    $this->setStringTranslation($translation);
    $this->setMessenger($messenger);
  }

  /**
   * Save an entry in the database.
   *
   * @param array $entry
   *   An array containing all the fields of the database record.
   *
   * @return int
   *   The number of updated rows.
   *
   * @throws \Exception
   *   When the database insert fails.
   */
  public function insert(array $entry) {
    if (empty($entry['created'])) {
      $entry['created'] = time();
    }

    if (empty($entry['updated'])) {
      $entry['updated'] = time();
    }

    try {
      $return_value = $this->connection->insert('entity_share_subscription_record')
        ->fields($entry)
        ->execute();
    }
    catch (\Exception $e) {
      $this->messenger()->addMessage($this->t('Insert failed. Message = %message', [
        '%message' => $e->getMessage(),
      ]), 'error');
    }
    return $return_value ?? NULL;
  }

  /**
   * Update an entry in the database.
   *
   * @param array $entry
   *   An array containing all the fields of the item to be updated.
   *
   * @return int
   *   The number of updated rows.
   */
  public function update(array $entry) {
    if (empty($entry['updated'])) {
      $entry['updated'] = time();
    }

    try {
      $count = $this->connection->update('entity_share_subscription_record')
        ->fields($entry)
        ->condition('id', $entry['id'])
        ->execute();
    }
    catch (\Exception $e) {
      $this->messenger()->addMessage($this->t('Update failed. Message = %message, query= %query', [
        '%message' => $e->getMessage(),
        '%query' => $e->query_string,
      ]
      ), 'error');
    }
    return $count ?? 0;
  }

  /**
   * Delete an entry from the database.
   *
   * @param int $id
   *   The identifier of removing entry.
   *
   * @see Drupal\Core\Database\Connection::delete()
   */
  public function delete($id) {
    $this->connection->delete('entity_share_subscription_record')
      ->condition('id', $id)
      ->execute();
  }

  /**
   * Read from the database using a filter array.
   *
   * @param array $entry
   *   An array containing all the fields used to search the entries in the
   *   table.
   *
   * @return object
   *   An object containing the loaded entries if found.
   *
   * @see Drupal\Core\Database\Connection::select()
   */
  public function load(array $entry = []) {
    $select = $this->connection
      ->select('entity_share_subscription_record')
      // Add all the fields into our select query.
      ->fields('entity_share_subscription_record');

    foreach ($entry as $field => $value) {
      $select->condition($field, $value);
    }
    return $select->execute()->fetchObject();
  }

  /**
   * Load database entry by secret field.
   *
   * @param string $subscription_id
   *   THe identifier of subscription.
   *
   * @return object
   *   THe entry object.
   */
  public function loadBySubscriptionId($subscription_id) {
    $select = $this->connection
      ->select('entity_share_subscription_record')
      ->fields('entity_share_subscription_record');
    $select->condition('subscription_id', $subscription_id);
    return $select->execute()->fetchObject();
  }

}
