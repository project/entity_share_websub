<?php

namespace Drupal\entity_share_websub_subscriber\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Entity Share Websub Subscriber settings form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_share_websub_subscriber_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'entity_share_websub_subscriber.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('entity_share_websub_subscriber.settings');

    $form['import_config'] = [
      '#type' => 'select',
      '#title' => $this->t('Import config'),
      '#options' => $this->getImportConfigOptions(),
      '#default_value' => $config->get('import_config'),
      '#empty_value' => '',
      '#required' => TRUE,
    ];

    $form['hide_default_button'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide default button'),
      '#description' => $this->t('Hide button provided by Entity Share.'),
      '#default_value' => $config->get('hide_default_button'),
    ];

    $form['break_subscription_on_edit'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ask to cancel subscription'),
      '#description' => $this->t('Ask user about cancelling of subscription after editing of imported content.'),
      '#default_value' => $config->get('break_subscription_on_edit'),
    ];

    $form['subscribe_hub_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subscribe Hub Url'),
      '#description' => $this->t('Set the subscribe endpoint of the Hub if custom. Default is: <i>/subscribe</i>'),
      '#default_value' => $config->get('subscribe_hub_url') != '' ? $config->get('subscribe_hub_url') : '/subscribe',
      '#required' => TRUE,
    ];

    $form['cancel'] = [
      '#type' => 'details',
      '#title' => $this->t('Texts for the cancelling of the subscription'),
    ];

    $form['cancel']['cancel_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $config->get('cancel_title'),
    ];

    $form['cancel']['cancel_description'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Description'),
      '#format' => $config->get('cancel_description')['format'],
      '#default_value' => $config->get('cancel_description')['value'],
    ];

    $form['cancel']['cancel_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cancel text'),
      '#default_value' => $config->get('cancel_text'),
    ];

    $form['cancel']['confirm_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Confirm text'),
      '#default_value' => $config->get('confirm_text'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('entity_share_websub_subscriber.settings');
    $config->set('hide_default_button', $form_state->getValue('hide_default_button'));
    $config->set('break_subscription_on_edit', $form_state->getValue('break_subscription_on_edit'));
    $config->set('import_config', $form_state->getValue('import_config'));
    $config->set('cancel_title', $form_state->getValue('cancel_title'));
    $config->set('cancel_description', $form_state->getValue('cancel_description'));
    $config->set('cancel_text', $form_state->getValue('cancel_text'));
    $config->set('confirm_text', $form_state->getValue('confirm_text'));
    $config->set('subscribe_hub_url', $form_state->getValue('subscribe_hub_url'));
    $config->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Returns the options for the import config select element.
   *
   * @return array
   *   The list of available import configs.
   */
  protected function getImportConfigOptions() {
    $options = [];
    // An array of import configs.
    $import_configs = $this->entityTypeManager->getStorage('import_config')
      ->loadMultiple();
    foreach ($import_configs as $import_config) {
      $options[$import_config->id()] = $import_config->label();
    }
    return $options;
  }

}
