<?php

namespace Drupal\entity_share_websub_subscriber\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\entity_share_websub_subscriber\BatchHelper;
use Drupal\entity_share_websub_subscriber\Subscriber;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Defines a confirmation form to confirm deletion of something by id.
 */
class CancelSubscriptionForm extends ConfirmFormBase {

  /**
   * The subscription repository.
   *
   * @var \Drupal\entity_share_websub_subscriber\SubscriptionRepository
   */
  protected $subscriptionRepository;

  /**
   * The subscription record repository.
   *
   * @var \Drupal\entity_share_websub_subscriber\SubscriptionRecordRepository
   */
  protected $subscriptionRecordRepository;

  /**
   * The subscription manager.
   *
   * @var \Drupal\entity_share_websub_subscriber\SubscriptionManager
   */
  protected $subscriptionManager;

  /**
   * The subscription row.
   *
   * @var array
   */
  protected $subscription;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Configuration Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Config of the entity_share_websub_subscriber module.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->subscriptionRepository = $container->get('entity_share_websub_subscriber.subscription_repository');
    $instance->subscriptionRecordRepository = $container->get('entity_share_websub_subscriber.subscription_record_repository');
    $instance->subscriptionManager = $container->get('entity_share_websub_subscriber.subscription_manager');
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->configFactory = $container->get('config.factory');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $id = NULL) {
    $this->subscription = $this->loadSubscription($id);
    $this->config = $this->configFactory->get('entity_share_websub_subscriber.settings');

    if (empty($this->subscription) || $this->subscription->status != Subscriber::SUBSCRIPTION_VERIFIED) {
      throw new NotFoundHttpException();
    }

    $form = parent::buildForm($form, $form_state);
    $form['actions']['submit']['#button_type'] = 'light';
    $form['actions']['cancel']['#attributes'] = [
      'class' => ['button', 'button--primary'],
    ];
    // Switch positions of buttons.
    $form['actions']['submit']['#weight'] = 5;
    $form['actions']['cancel']['#weight'] = -5;

    return $form;
  }

  /**
   * Load subscription database row.
   *
   * @param int $id
   *   The identifier of the subscription.
   *
   * @return object
   *   The subscription row.
   */
  protected function loadSubscription($id) {
    return $this->subscriptionRepository->load([
      'id' => $id,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $result = $this->subscriptionManager->unsubscribe(
      $this->subscription->remote_id,
      $this->subscription->channel_id,
      $this->subscription->uuid
    );

    // Hub notifying.
    if ($result) {
      // @todo Fix problem of a blocking process here. Throw an event and use a
      //   subscriber to move to post request.
      BatchHelper::notifyHub(
        $this->subscription->remote_id,
        $this->subscription->channel_id,
        $this->subscription->uuid,
        $this->subscription->subscription_key,
        Subscriber::MODE_UNSUBSCRIBE
      );
    }

    $form_state->setRedirect('system.admin_content');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_share_websub_subscriber_confirm_unsubscribe_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('system.admin_content');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->config->get('cancel_title');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    $description = $this->config->get('cancel_description')['value'];

    // Replace date placeholder with the subscription date.
    $subscription_record = $this->subscriptionRecordRepository->loadBySubscriptionId($this->subscription->id);
    $date = $this->t('Unknown');
    if (!empty($subscription_record)) {
      $date = $this->dateFormatter->format($subscription_record->updated, 'custom', 'm.d.Y');
    }

    return str_replace('[date]', $date, $description);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->config->get('confirm_text');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText() {
    return $this->config->get('cancel_text');
  }

}
