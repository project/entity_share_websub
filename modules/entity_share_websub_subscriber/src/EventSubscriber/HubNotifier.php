<?php

namespace Drupal\entity_share_websub_subscriber\EventSubscriber;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\Core\Queue\SuspendQueueException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\TerminateEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Process hub communications post-request.
 */
class HubNotifier implements EventSubscriberInterface {

  /**
   * The queue.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queue;

  /**
   * The queue manager.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $queueManager;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $loggerChannelFactory;

  /**
   * Constructs a new NotificationProcessor object.
   */
  public function __construct(QueueFactory $queue, QueueWorkerManagerInterface $queue_manager, LoggerChannelFactoryInterface $logger_channel_factory) {
    $this->queue = $queue;
    $this->queueManager = $queue_manager;
    $this->loggerChannelFactory = $logger_channel_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];
    $events[KernelEvents::TERMINATE] = ['kernelTerminate'];
    return $events;
  }

  /**
   * This method is called when the kernel.terminate is dispatched.
   *
   * @param \Symfony\Component\HttpKernel\Event\TerminateEvent $event
   *   The dispatched event.
   */
  public function kernelTerminate(TerminateEvent $event) {
    $process_timeout = ini_get('max_execution_time') ?: 30;
    $end = time() + $process_timeout - 5;

    $this->processQueue('entity_share_websub_subscriber_subscribe', $end);

    if (time() < $end) {
      $this->processQueue('entity_share_websub_subscriber_import', $end);
    }
  }

  /**
   * Process selected queue.
   *
   * @param string $queue_name
   *   Queue name to process.
   * @param int $end
   *   The end of the execution.
   */
  protected function processQueue($queue_name, $end) {
    $queue = $this->queue->get($queue_name);
    $worker = $this->queueManager->createInstance($queue_name);
    $log = $this->loggerChannelFactory->get('entity_share_websub_subscriber');

    while (time() < $end && ($item = $queue->claimItem())) {
      try {
        $worker->processItem($item->data);
        $queue->deleteItem($item);
      }
      catch (SuspendQueueException $e) {
        $queue->releaseItem($item);
        $log->error($e->getMessage());
        break;
      }
      catch (\Exception $e) {
        $queue->deleteItem($item);
        $log->error($e->getMessage());
      }
    }
  }

}
