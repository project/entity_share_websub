<?php

namespace Drupal\entity_share_websub_subscriber\EventSubscriber;

use Drupal\Core\Queue\QueueFactory;
use Drupal\entity_share_websub_subscriber\Event\ContentSyncEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Queues subscribed items for import.
 */
class ItemImport implements EventSubscriberInterface {

  /**
   * Queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queue;

  /**
   * Constructs a new ItemImport object.
   *
   * @param \Drupal\Core\Queue\QueueFactory $queue
   *   Queue factory service.
   */
  public function __construct(QueueFactory $queue) {
    $this->queue = $queue;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];
    $events[ContentSyncEvent::EVENT_SYNC] = ['syncSubscription'];
    return $events;
  }

  /**
   * This method is called when ContentSyncEvent::EVENT_SYNC is dispatched.
   *
   * @param \Drupal\entity_share_websub_subscriber\Event\ContentSyncEvent $event
   *   The dispatched event.
   */
  public function syncSubscription(ContentSyncEvent $event) {
    $this->createQueueItem($event->getSubscription());
  }

  /**
   * Add item to import queue.
   *
   * @param object $subscription
   *   The subscription object.
   */
  protected function createQueueItem($subscription) {
    $queue = $this->queue->get('entity_share_websub_subscriber_import');
    $item = [
      'uuid' => $subscription->uuid,
      'remote_id' => $subscription->remote_id,
      'channel_id' => $subscription->channel_id,
      'subscription_id' => $subscription->id,
    ];
    $queue->createItem($item);
  }

}
