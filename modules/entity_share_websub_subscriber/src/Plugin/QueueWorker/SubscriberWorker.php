<?php

namespace Drupal\entity_share_websub_subscriber\Plugin\QueueWorker;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\entity_share_websub_subscriber\Subscriber;
use Drupal\entity_share_websub_subscriber\SubscriptionRepository;
use GuzzleHttp\Exception\ConnectException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Asynchronous import queue worker.
 *
 * @QueueWorker(
 *   id = "entity_share_websub_subscriber_subscribe",
 *   title = @Translation("Processing hub notifications queue.")
 * )
 */
final class SubscriberWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    private LoggerChannelFactoryInterface $loggerChannelFactory,
    private Subscriber $subscriberService,
    private SubscriptionRepository $subscriptionRepository,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory'),
      $container->get('entity_share_websub_subscriber.subscriber'),
      $container->get('entity_share_websub_subscriber.subscription_repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($item) {

    $required = [
      'remote_id',
      'channel_id',
      'uuid',
      'mode',
      'subscription_key',
    ];

    // Check that all needed fields provided by queue item.
    foreach ($required as $required_field) {
      if (empty($item[$required_field])) {
        $this->loggerChannelFactory->get('entity_share_websub_subscriber')->error(
          "Subscriber queue item can not be processed because of missing @field",
          [
            '@field' => $required_field,
          ]
        );
        return;
      }
    }

    // Checking the subscription type.
    if (!in_array($item['mode'], [
      Subscriber::MODE_SUBSCRIBE,
      Subscriber::MODE_UNSUBSCRIBE,
    ])) {
      $this->loggerChannelFactory->get('entity_share_websub_subscriber')->error(
        "Subscriber queue item can not be processed as unknown mode provided"
      );
      return;
    }

    // Set data needed for Subscriber service.
    $this->subscriberService
      ->setRemoteId($item['remote_id'])
      ->setChannelId($item['channel_id'])
      ->setSubscriptionKey($item['subscription_key'])
      ->setUuid($item['uuid']);

    if (!empty($item['email'])) {
      $this->subscriberService->setEmail($item['email']);
    }

    if ($item['mode'] === Subscriber::MODE_SUBSCRIBE) {
      $this->processSubscribe($item);
    }
    else {
      $this->processUnsubscribe($item);
    }
  }

  /**
   * Processing subscribe item.
   *
   * @param array $item
   *   Queue item.
   */
  protected function processSubscribe(array $item) {
    $secret = Subscriber::getSecret($item['remote_id'], $item['channel_id'], $item['uuid']);
    $subscription = $this->subscriptionRepository->loadBySecret($secret);

    if ($subscription && $subscription->status === Subscriber::SUBSCRIPTION_VERIFIED) {
      $this->loggerChannelFactory->get('entity_share_websub_subscriber')->warning(
        "Subscription already exists for @uuid from channel @channel_id of remote @remote_id.",
        [
          '@uuid' => $item['uuid'],
          '@channel_id' => $item['channel_id'],
          '@remote_id' => $item['remote_id'],
        ]
      );
      return;
    }

    try {
      $result = $this->subscriberService->subscribe();
    }
    catch (ConnectException $e) {
      $this->loggerChannelFactory->get('entity_share_websub_subscriber')->error(
        "Unable to connect the hub. Queue processing stopped."
      );
      throw new SuspendQueueException();
    }
    catch (\Exception $e) {
      $this->loggerChannelFactory->get('entity_share_websub_subscriber')->error(
        "Unable to process subscription request for @uuid from channel @channel_id of remote @remote_id: @error",
        [
          '@uuid' => $item['uuid'],
          '@channel_id' => $item['channel_id'],
          '@remote_id' => $item['remote_id'],
          '@error' => $e->getMessage(),
        ]
      );
      return;
    }

    if (empty($result)) {
      $this->loggerChannelFactory->get('entity_share_websub_subscriber')->warning(
        "Hub declined subscription request for @uuid from channel @channel_id of remote @remote_id.",
        [
          '@uuid' => $item['uuid'],
          '@channel_id' => $item['channel_id'],
          '@remote_id' => $item['remote_id'],
        ]
      );
      return;
    }
  }

  /**
   * Processing unsubscribe item.
   *
   * @param array $item
   *   Queue item.
   */
  protected function processUnsubscribe(array $item) {
    $secret = Subscriber::getSecret($item['remote_id'], $item['channel_id'], $item['uuid']);
    $subscription = $this->subscriptionRepository->loadBySecret($secret);

    if (empty($subscription)) {
      $this->loggerChannelFactory->get('entity_share_websub_subscriber')->error(
        "Unable ot unsubscribe from @uuid from channel @channel_id of remote @remote_id. Subscription doesn't exist.",
        [
          '@uuid' => $item['uuid'],
          '@channel_id' => $item['channel_id'],
          '@remote_id' => $item['remote_id'],
        ]
      );
      return;
    }

    if ($subscription->status === Subscriber::SUBSCRIPTION_CANCELLED) {
      $this->loggerChannelFactory->get('entity_share_websub_subscriber')->error(
        "Unable ot unsubscribe from @uuid from channel @channel_id of remote @remote_id. Subscription already canceled.",
        [
          '@uuid' => $item['uuid'],
          '@channel_id' => $item['channel_id'],
          '@remote_id' => $item['remote_id'],
        ]
      );
      return;
    }

    try {
      $result = $this->subscriberService->unsubscribe();
    }
    catch (\Exception $e) {
      $this->loggerChannelFactory->get('entity_share_websub_subscriber')->error(
        "Unable to process subscription request for @uuid from channel @channel_id of remote @remote_id.",
        [
          '@uuid' => $item['uuid'],
          '@channel_id' => $item['channel_id'],
          '@remote_id' => $item['remote_id'],
        ]
      );
      return;
    }

    if (empty($result)) {
      $this->loggerChannelFactory->get('entity_share_websub_subscriber')->warning(
        "Hub declined unsubscribe request for @uuid from channel @channel_id of remote @remote_id.",
        [
          '@uuid' => $item['uuid'],
          '@channel_id' => $item['channel_id'],
          '@remote_id' => $item['remote_id'],
        ]
      );
      return;
    }
  }

}
