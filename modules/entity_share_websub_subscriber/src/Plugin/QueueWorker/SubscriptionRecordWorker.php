<?php

namespace Drupal\entity_share_websub_subscriber\Plugin\QueueWorker;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\entity_share_client\ImportContext;
use Drupal\entity_share_client\Service\ImportServiceInterface;
use Drupal\entity_share_client\Service\RemoteManagerInterface;
use Drupal\entity_share_websub_subscriber\SubscriptionRecordRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Asynchronous import queue worker.
 *
 * @QueueWorker(
 *   id = "entity_share_websub_subscriber_import",
 *   title = @Translation("Entity Share asynchronous import")
 * )
 */
final class SubscriptionRecordWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    private LoggerChannelFactoryInterface $loggerChannelFactory,
    private EntityTypeManagerInterface $entityTypeManager,
    private RemoteManagerInterface $remoteManager,
    private SubscriptionRecordRepository $subscriptionRecordRepository,
    private ImportServiceInterface $importService,
    private ConfigFactoryInterface $configFactory,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_share_client.remote_manager'),
      $container->get('entity_share_websub_subscriber.subscription_record_repository'),
      $container->get('entity_share_client.import_service'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($item) {

    $required = [
      'remote_id',
      'channel_id',
      'uuid',
      'subscription_id',
    ];

    // Check that all needed fields provided by queue item.
    foreach ($required as $required_field) {
      if (empty($item[$required_field])) {
        $this->loggerChannelFactory->get('entity_share_subscriber')->error(
          "Subscriber queue item can not be processed because of missing @field data",
          [
            '@field' => $required_field,
          ]
        );
        return;
      }
    }

    // Getting the name of the import config for the following usage.
    $import_config = $this->getImportConfigName();
    if (empty($import_config)) {
      $error = 'Please, specify import config in websub settings';
      $this->loggerChannelFactory->get('entity_share_subscriber')->error($error);
      return;
    }

    /** @var \Drupal\entity_share_client\Entity\RemoteInterface $remote */
    $remote = $this->entityTypeManager->getStorage('remote')->load($item['remote_id']);
    $channel_infos = $this->remoteManager->getChannelsInfos($remote);
    $entity_type = $channel_infos[$item['channel_id']]['channel_entity_type'];
    $bundle = $channel_infos[$item['channel_id']]['channel_bundle'];

    // Create import context and run import outside of the batch context.
    $import_context = new ImportContext(
      $item['remote_id'],
      $item['channel_id'],
      $import_config
    );
    $entity_id = $this->importService->importEntities($import_context, [$item['uuid']], FALSE);

    if (empty($entity_id)) {
      $this->loggerChannelFactory->get('entity_share_subscriber')->warning(
        "Cannot synchronize item @uuid from channel @channel_id of remote @remote_id using @import_config_id",
        [
          '@uuid' => $item['uuid'],
          '@channel_id' => $item['channel_id'],
          '@remote_id' => $item['remote_id'],
          '@import_config_id' => $import_config,
        ]
      );
      return;
    }

    $existed_record = $this->subscriptionRecordRepository->loadBySubscriptionId($item['subscription_id']);

    if (empty($existed_record)) {
      $this->subscriptionRecordRepository->insert([
        'subscription_id' => $item['subscription_id'],
        'entity_type' => str_replace('-', '_', $entity_type),
        'entity_bundle' => str_replace('-', '_', $bundle),
        'entity_id' => $entity_id[$item['uuid']],
      ]);
    }
    else {
      $this->subscriptionRecordRepository->update(['id' => $existed_record->id]);
    }
  }

  /**
   * Gets the name of the import configuration.
   *
   * @return string|null
   *   The name of the import configuration.
   */
  protected function getImportConfigName() {
    $config = $this->configFactory->get('entity_share_websub_subscriber.settings');
    return $config->get('import_config');
  }

}
