<?php

namespace Drupal\entity_share_websub_subscriber;

/**
 * Static methods for batch processes.
 */
class BatchHelper {

  /**
   * Process the subscription item.
   *
   * @param string $remote_id
   *   The remote identifier.
   * @param string $channel_id
   *   The channel identifier.
   * @param string $uuid
   *   The UUID of the entity.
   * @param string $mode
   *   The subscription mode.
   * @param object $context
   *   Context for operations.
   */
  public static function processItem($remote_id, $channel_id, $uuid, $mode, &$context) {
    $manager = \Drupal::service('entity_share_websub_subscriber.subscription_manager');

    if ($mode == Subscriber::MODE_SUBSCRIBE) {
      $result = $manager->subscribe($remote_id, $channel_id, $uuid);
    }
    else {
      $result = $manager->unsubscribe($remote_id, $channel_id, $uuid);
    }

    if ($result) {
      // @todo Fix problem of trying to do to many things here. Create an event
      //   and use event subscriber instead.
      $record = $manager->getSubscription($remote_id, $channel_id, $uuid);
      self::notifyHub($remote_id, $channel_id, $uuid, $record->subscription_key, $mode);
    }

    $context['results'][] = $uuid;
  }

  /**
   * Notify hub about subscription status.
   *
   * @param string $remote_id
   *   The remote identifier.
   * @param string $channel_id
   *   The channel identifier.
   * @param string $uuid
   *   The UUID of the entity.
   * @param string $subscription_key
   *   The subscription key.
   * @param string $mode
   *   The subscription mode.
   */
  public static function notifyHub($remote_id, $channel_id, $uuid, $subscription_key, $mode) {
    $subscriber = \Drupal::service('entity_share_websub_subscriber.subscriber');
    $email = \Drupal::currentUser()->getEmail();

    $subscriber
      ->setRemoteId($remote_id)
      ->setChannelId($channel_id)
      ->setUuid($uuid)
      ->setSubscriptionKey($subscription_key)
      ->setEmail($email);

    try {
      if ($mode == Subscriber::MODE_UNSUBSCRIBE) {
        $subscriber->unsubscribe();
      }
      else {
        $subscriber->subscribe();
      }
    }
    catch (\Exception $e) {
      // If hub is not available then we put it in the queue
      // and handle async using terminate event.
      /** @var \Drupal\Core\Queue\QueueInterface $queue */
      $queue = \Drupal::service('queue')->get('entity_share_websub_subscriber_subscribe');
      $queue->createItem([
        'uuid' => $uuid,
        'remote_id' => $remote_id,
        'channel_id' => $channel_id,
        'subscription_key' => $subscription_key,
        'email' => $email,
        'mode' => $mode,
      ]);
    }
  }

  /**
   * Finished callback for batch.
   */
  public static function finished($success, $results, $operations) {
    $messenger = \Drupal::messenger();
    if ($success) {
      $messenger->addMessage(t('@count results processed.', ['@count' => count($results)]));
    }
    else {
      // An error occurred.
      $error_operation = reset($operations);
      $messenger->addMessage(
        t('An error occurred while processing @operation with arguments : @args',
          [
            '@operation' => $error_operation[0],
            '@args' => print_r($error_operation[0], TRUE),
          ]
        )
      );
    }
  }

}
