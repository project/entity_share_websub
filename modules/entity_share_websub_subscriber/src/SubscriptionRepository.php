<?php

namespace Drupal\entity_share_websub_subscriber;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Storage service for subscriptions.
 */
class SubscriptionRepository {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Construct a repository object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translation
   *   The translation service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(Connection $connection, TranslationInterface $translation, MessengerInterface $messenger) {
    $this->connection = $connection;
    $this->setStringTranslation($translation);
    $this->setMessenger($messenger);
  }

  /**
   * Save an entry in the database.
   *
   * @param array $entry
   *   An array containing all the fields of the database record.
   *
   * @return int
   *   The number of updated rows.
   *
   * @throws \Exception
   *   When the database insert fails.
   */
  public function insert(array $entry) {
    if (empty($entry['created'])) {
      $entry['created'] = time();
    }

    try {

      $return_value = $this->connection->insert('entity_share_subscription')
        ->fields($entry)
        ->execute();
    }
    catch (\Exception $e) {
      $this->messenger()->addMessage($this->t('Insert failed. Message = %message', [
        '%message' => $e->getMessage(),
      ]), 'error');
    }
    return $return_value ?? NULL;
  }

  /**
   * Update an entry in the database.
   *
   * @param array $entry
   *   An array containing all the fields of the item to be updated.
   *
   * @return int
   *   The number of updated rows.
   */
  public function update(array $entry) {
    try {
      $count = $this->connection->update('entity_share_subscription')
        ->fields($entry)
        ->condition('id', $entry['id'])
        ->execute();
    }
    catch (\Exception $e) {
      $this->messenger()->addMessage($this->t('Update failed. Message = %message, query = %query', [
        '%message' => $e->getMessage(),
        '%query' => $e->query_string,
      ]
      ), 'error');
    }
    return $count ?? 0;
  }

  /**
   * Delete an entry from the database.
   *
   * @param int $id
   *   The identifier of removing entry.
   *
   * @see Drupal\Core\Database\Connection::delete()
   */
  public function delete($id) {
    $this->connection->delete('entity_share_subscription')
      ->condition('id', $id)
      ->execute();
  }

  /**
   * Read from the database using a filter array.
   *
   * @param array $entry
   *   An array containing all the fields used to search the entries in the
   *   table.
   *
   * @return object
   *   An object containing the loaded entries if found.
   *
   * @see Drupal\Core\Database\Connection::select()
   */
  public function load(array $entry = []) {
    $select = $this->connection
      ->select('entity_share_subscription')
      // Add all the fields into our select query.
      ->fields('entity_share_subscription');

    foreach ($entry as $field => $value) {
      $select->condition($field, $value);
    }
    return $select->execute()->fetchObject();
  }

  /**
   * Load database entry by secret field.
   *
   * @param string $secret
   *   The secret of subscription.
   *
   * @return object
   *   An object containing the loaded entries if found.
   */
  public function loadBySecret($secret) {
    $select = $this->connection
      ->select('entity_share_subscription')
      ->fields('entity_share_subscription');
    $select->condition('secret', $secret);
    return $select->execute()->fetchObject();
  }

  /**
   * Load database entry by subscription_key field.
   *
   * @param string $subscription_key
   *   The subscription key.
   *
   * @return object
   *   An object containing the loaded entries if found.
   */
  public function loadBySubscriptionKey($subscription_key) {
    $select = $this->connection
      ->select('entity_share_subscription')
      ->fields('entity_share_subscription');
    $select->condition('subscription_key', $subscription_key);
    return $select->execute()->fetchObject();
  }

  /**
   * Load all subscriptions for selected channel.
   *
   * @param string $remote_id
   *   The remote identifier.
   * @param string $channel_id
   *   The channel identifier.
   *
   * @return object[]
   *   The list of subscriptions per remote/channel.
   */
  public function loadSubscriptionsByChannel($remote_id, $channel_id) {
    $select = $this->connection
      ->select('entity_share_subscription')
      ->fields('entity_share_subscription');
    $select->condition('remote_id', $remote_id);
    $select->condition('channel_id', $channel_id);

    return $select->execute()->fetchAll();
  }

  /**
   * Load subscription object for selected entity through subscription record.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The subscribed entity.
   *
   * @return object
   *   An object containing the loaded entries if found.
   */
  public function loadSubscriptionByEntity(EntityInterface $entity) {
    $query = $this->connection->select('entity_share_subscription', 's');
    $query->join('entity_share_subscription_record', 'sr', 'sr.subscription_id = s.id');
    $query->fields('s')
      ->condition('sr.entity_type', $entity->getEntityTypeId())
      ->condition('sr.entity_bundle', $entity->bundle())
      ->condition('sr.entity_id', $entity->id());

    return $query->execute()->fetchObject();
  }

}
