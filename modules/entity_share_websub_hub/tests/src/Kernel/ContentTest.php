<?php

namespace Drupal\Tests\entity_share_websub_hub\Kernel;

use Drupal\entity_share_server\Entity\Channel;
use Drupal\entity_share_websub_hub\Hub;
use Drupal\entity_share_websub_hub\Subscription;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\Tests\entity_share_websub_hub\Traits\ValuesTrait;

/**
 * Kernel tests for the hub - content.
 *
 * @group entity_share_websub_hub
 */
class ContentTest extends EntityKernelTestBase {
  use ValuesTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'system',
    'contact',
    'field',
    'file',
    'user',
    'entity_share_websub_hub',
    'node',
    'entity_share_server',
    'jsonapi',
    'serialization',
  ];

  /**
   * A Subscription object.
   *
   * @var \Drupal\entity_share_websub_hub\Subscription
   */
  protected $subscription;

  /**
   * A database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Email validator utility.
   *
   * @var \Drupal\Component\Utility\EmailValidator
   */
  protected $emailValidator;

  /**
   * Node ID.
   *
   * @var int
   */
  protected $nodeId;


  /**
   * Query factory service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The test user.
   *
   * @var \Drupal\user\Entity\User|false
   */
  protected $user;

  /**
   * Entity Repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installSchema('entity_share_websub_hub', Subscription::TABLE_NAME);
    $this->installSchema('node', ['node_access']);
    $this->installEntitySchema('node');
    $this->installEntitySchema('channel');

    Channel::create([
      'id' => 'test_channel',
      'label' => 'Test Channel',
      'channel_entity_type' => 'node',
      'channel_bundle' => 'article',
    ])->save();

    $this->setUpValues();
    $this->user = $this->drupalCreateUser(['administer site configuration']);
    $node = Node::create([
      'type' => 'article',
      'status' => 1,
      'uid' => $this->user->id(),
      'title' => 'Test node',
    ]);
    $node->save();
    $this->nodeId = $node->id();
    $this->database = \Drupal::service('database');
    $this->emailValidator = \Drupal::service('email.validator');
    $this->entityTypeManager = \Drupal::service('entity_type.manager');
    $this->entityRepository = \Drupal::service('entity.repository');
    $this->subscription = new Subscription($this->database, $this->emailValidator, $this->entityTypeManager, $this->entityRepository);
    $topic = 'test_channel/' . $node->uuid();
    $this->subscriptionItem = $this->subscription->save($topic, $this->mockSubscriber, $this->mockSecret, $this->mockEmail, 'subscribe');
    $this->queueFactory = \Drupal::service('queue');
  }

  /**
   * Test update cases.
   */
  public function testUpdate() {
    $node = Node::load($this->nodeId);
    $node->set('title', 'Test node 2');
    $node->save();
    $queue = $this->queueFactory->get(Hub::QUEUE_NAME);
    $item = $queue->claimItem();
    $this->assertEquals(FALSE, $item, 'No update on unverified subscriptions');

    $items = $this->subscription->checkSubscriptions($node);
    $this->assertEmpty($items, 'Unverified items not found');

    $this->subscription->verifySubscription($this->mockSubscriber);
    $items = $this->subscription->checkSubscriptions($node);
    $this->assertNotEmpty($items, 'Verified items found');

    $node->set('title', 'Test node 3');
    $node->save();
    $item = $queue->claimItem();
    $this->assertNotEmpty($item, 'Update on verified subscriptions');

    $queue->deleteItem($item);
    $node->set('status', 0);
    $node->save();
    $item = $queue->claimItem();
    $this->assertNotEmpty($item, 'Update on node unpublish');
    $this->assertEquals($item->data['action'], Hub::ACTION_CANCEL, 'Correct cancel update');

    $items = $this->subscription->checkSubscriptions($node);
    $this->assertEmpty($items, 'The subscription set inactive on unpublish');

    $this->subscription->updateSubscriptions([1], ['status' => Subscription::STATUS_ACTIVE]);
    $items = $this->subscription->checkSubscriptions($node);
    $this->assertNotEmpty($items, 'Successfully restored status');

    $node->delete();
    $item = $queue->claimItem();
    $this->assertNotEmpty($item, 'Update on node deletion');
    $this->assertEquals($item->data['action'], Hub::ACTION_CANCEL, 'Correct cancel update for deletion');

    $updates = $this->subscription->getUpdates($this->mockSubscriber, TRUE);
    $this->assertNotEmpty($updates, 'Existing cancel updates when node deleted');

  }

}
