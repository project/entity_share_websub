<?php

namespace Drupal\Tests\entity_share_websub_hub\Kernel;

use Drupal\entity_share_server\Entity\Channel;
use Drupal\entity_share_websub_hub\Subscription;
use Drupal\KernelTests\KernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\Tests\entity_share_websub_hub\Traits\ValuesTrait;

/**
 * Kernel tests for the hub - subscription.
 *
 * @group entity_share_websub_hub
 */
class SubscriptionTest extends KernelTestBase {
  use ValuesTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'user',
    'file',
    'entity_share_websub_hub',
    'entity_share_server',
    'jsonapi',
    'serialization',
  ];

  /**
   * Subscription object.
   *
   * @var \Drupal\entity_share_websub_hub\Subscription
   */
  protected $subscription;

  /**
   * Database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Email validator service.
   *
   * @var \Drupal\Component\Utility\EmailValidator
   */
  protected $emailValidator;

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Entity Repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('node');
    $this->installEntitySchema('channel');
    $this->installEntitySchema('user');

    Channel::create([
      'id' => 'test_channel',
      'label' => 'Test Channel',
      'channel_entity_type' => 'node',
      'channel_bundle' => 'article',
    ])->save();

    $this->installSchema('entity_share_websub_hub', Subscription::TABLE_NAME);
    $this->database = \Drupal::service('database');
    $this->emailValidator = \Drupal::service('email.validator');
    $this->entityTypeManager = \Drupal::service('entity_type.manager');
    $this->entityRepository = \Drupal::service('entity.repository');
    $this->subscription = new Subscription($this->database, $this->emailValidator, $this->entityTypeManager, $this->entityRepository);
    $this->setUpValues();
    Node::create([
      'vid' => 1,
      'type' => 'article',
      'uuid' => '7cc7074e-a6ed-4a08-891d-8d1aedf196dc',
      'langcode' => 'en',
      'title' => 'Test node',
    ])->save();
  }

  /**
   * Test subscription saving.
   */
  public function testSave() {
    // cSpell:disable-next-line.
    [$code, $result] = $this->subscription->save($this->mockTopic, $this->mockSubscriber, $this->mockSecret, 'asdasdsdsfdsf', 'subscribe');
    $this->assertEquals(400, $code, 'Test return code (email error)');
    $this->assertEquals('Provided email is not valid', $result, 'Test return text (email error)');
    [$code, $result] = $this->subscription->save('monster@sesame.street', $this->mockSubscriber, $this->mockSecret, $this->mockEmail, 'subscribe');
    $this->assertEquals(400, $code, 'Test return code (faulty topic)');
    $this->assertEquals('Incorrect topic format', $result, 'Test return text (faulty topic)');
    [$code, $result] = $this->subscription->save($this->mockTopic, '/local', $this->mockSecret, $this->mockEmail, 'subscribe');
    $this->assertEquals(400, $code, 'Test return code (local subscriber)');
    $this->assertEquals('Provided callback is not a valid URL', $result, 'Test return text (local subscriber)');
    [$code, $result] = $this->subscription->save($this->mockTopic, $this->mockSubscriber, '', $this->mockEmail, 'subscribe');
    $this->assertEquals(400, $code, 'Test return code (no secret)');
    $this->assertEquals('No secret provided', $result, 'Test return text (no secret)');
    [$code, $result] = $this->subscription->save($this->mockTopic, $this->mockSubscriber, $this->mockSecret, $this->mockEmail, 'scribe');
    $this->assertEquals(400, $code, 'Test return code (wrong mode)');
    $this->assertEquals('Unrecognized mode', $result, 'Test return text (wrong mode)');
    [
      $code,
    ] = $this->subscription->save($this->mockTopic, $this->mockSubscriber, $this->mockSecret, $this->mockEmail, 'subscribe');
    $this->assertEquals(202, $code, 'Test return code');
  }

  /**
   * Tests the checkSubscription() method.
   */
  public function testCheck() {
    $this->subscription->save($this->mockTopic, $this->mockSubscriber, $this->mockSecret, $this->mockEmail, 'subscribe');
    $result = $this->subscription->checkSubscriptions($this->mockNode);
    $this->assertEquals($result, [], 'Check existing but not verified');
    $this->subscription->verifySubscription($this->mockSubscriber);
    $result = $this->subscription->checkSubscriptions($this->mockNode);
    $this->assertEquals($result, ['1'], 'Check existing');
    $this->subscription->updateSubscriptions(
      $result,
      ['content_summary' => 'New test title : article', 'uid' => 1]
    );
    $updates = $this->subscription->getUpdates();
    $this->assertEquals($updates, [
      $this->mockSubscriber => [
        $this->mockTopic,
        $this->mockSecret,
      ],
    ], 'Check updates');
    $subscribers = $this->subscription->getSubscribersBySids([1]);
    $this->assertEquals(reset($subscribers), $this->mockSubscriber, 'Check subscribers by sids');
  }

}
