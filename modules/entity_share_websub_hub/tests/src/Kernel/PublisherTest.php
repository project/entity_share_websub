<?php

namespace Drupal\Tests\entity_share_websub_hub\Kernel;

use Drupal\entity_share_server\Entity\Channel;
use Drupal\entity_share_websub_hub\Hub;
use Drupal\entity_share_websub_hub\Publisher;
use Drupal\entity_share_websub_hub\Subscription;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\Tests\entity_share_websub_hub\Traits\ValuesTrait;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

/**
 * Kernel tests for the hub - publisher.
 *
 * @group entity_share_websub_hub
 */
class PublisherTest extends EntityKernelTestBase {
  use ValuesTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'user',
    'file',
    'entity_share_websub_hub',
    'entity_share_server',
    'jsonapi',
    'serialization',
  ];

  /**
   * The subscription service.
   *
   * @var \Drupal\entity_share_websub_hub\Subscription
   */
  protected $subscription;

  /**
   * A Publisher object.
   *
   * @var \Drupal\entity_share_websub_hub\Publisher
   */
  protected $publisher;

  /**
   * A Hub object.
   *
   * @var \Drupal\entity_share_websub_hub\Hub
   */
  protected $hub;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * A request object.
   *
   * @var \GuzzleHttp\Psr7\Request
   */
  public $currentRequest;

  /**
   * Http client mock.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClientMock;

  /**
   * The test user.
   *
   * @var \Drupal\user\Entity\User|false
   */
  protected $user;

  /**
   * Create a mock and queue two responses.
   */
  protected function initHttpClientMock(): void {
    $mock = new MockHandler([
      new Response(202),
    ]);

    $handlerStack = HandlerStack::create($mock);
    $this->httpClientMock = new Client(['handler' => $handlerStack]);
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('node');
    $this->installEntitySchema('channel');

    Channel::create([
      'id' => 'test_channel',
      'label' => 'Test Channel',
      'channel_entity_type' => 'node',
      'channel_bundle' => 'article',
    ])->save();

    $this->installSchema('entity_share_websub_hub', Subscription::TABLE_NAME);
    $this->subscription = \Drupal::service('entity_share_websub_hub.subscription');
    $this->queueFactory = \Drupal::service('queue');
    $this->initHttpClientMock();
    $this->hub = new Hub(
      $this->subscription,
      $this->queueFactory,
      $this->httpClientMock,
      \Drupal::service('logger.factory')
    );
    $this->publisher = new Publisher($this->hub, $this->subscription);
    $this->setUpValues();
    $this->user = $this->drupalCreateUser(['administer site configuration']);
    $node = Node::create([
      'type' => 'article',
      'status' => 1,
      'uid' => $this->user->id(),
      'title' => 'Test node',
    ]);
    $node->save();
    $this->mockNode = $node;
    $this->mockTopic = 'test_channel/' . $node->uuid();
  }

  /**
   * Test subscription notifications.
   */
  public function testNotifyRelevant() {
    $this->publisher->notifyRelevant($this->mockNode);
    $queue = $this->queueFactory->get(Hub::QUEUE_NAME);
    $item = $queue->claimItem();
    $this->assertEquals($item, FALSE, 'No pending subscriptions test');

    $this->subscription->save($this->mockTopic, $this->mockSubscriber, $this->mockSecret, $this->mockEmail, 'subscribe');
    $this->publisher->notifyRelevant($this->mockNode);
    $item = $queue->claimItem();
    $this->assertEquals($item, FALSE, 'No verified subscriptions test');

    $this->subscription->verifySubscription($this->mockSubscriber);
    $this->publisher->notifyRelevant($this->mockNode);
    $item = $queue->claimItem();
    $this->assertNotEquals($item, FALSE, 'Records notified and queued');
    $this->assertEquals($item->data, [
      'path' => $this->mockSubscriber,
    ]);
  }

}
