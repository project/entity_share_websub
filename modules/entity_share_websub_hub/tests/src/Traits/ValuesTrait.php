<?php

namespace Drupal\Tests\entity_share_websub_hub\Traits;

use Drupal\node\NodeInterface;

/**
 * Prepares mock values for tests.
 */
trait ValuesTrait {

  /**
   * A mock topic path.
   *
   * @var string
   */
  protected $mockTopic;

  /**
   * A mock secret value.
   *
   * @var string
   */
  protected $mockSecret;

  /**
   * A mock subscription url.
   *
   * @var string
   */
  protected $mockSubscriber;

  /**
   * A mock email address.
   *
   * @var string
   */
  protected $mockEmail;

  /**
   * A mock node.
   *
   * @var \PHPUnit\Framework\MockObject\MockObject|\Drupal\node\NodeInterface
   */
  protected $mockNode;

  /**
   * Prepare the values.
   */
  protected function setUpValues() {
    $this->mockSubscriber = 'http://www.test.example/subscribe/5536df06b5dcb966edab3a4c4d56213c16a8184';
    $this->mockTopic = 'test_channel/7cc7074e-a6ed-4a08-891d-8d1aedf196dc';
    $this->mockSecret = '5536df06b5dcbabaedab3a4c4d56213c16a8184';
    $this->mockEmail = 'test@test.example';
    $this->mockNode = $this->createMock(NodeInterface::class);
    $this->mockNode->method('uuid')->willReturn('7cc7074e-a6ed-4a08-891d-8d1aedf196dc');
    $this->mockNode->method('getEntityTypeId')->willReturn('node');
  }

}
