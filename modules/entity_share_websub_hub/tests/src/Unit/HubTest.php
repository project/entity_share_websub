<?php

namespace Drupal\Tests\entity_share_websub_hub\Unit;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueInterface;
use Drupal\entity_share_websub\SignatureTrait;
use Drupal\entity_share_websub_hub\Hub;
use Drupal\entity_share_websub_hub\SubscriptionInterface;
use Drupal\Tests\entity_share_websub_hub\Traits\ValuesTrait;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

/**
 * Unit tests for the hub.
 *
 * @group entity_share_websub_hub
 */
class HubTest extends UnitTestCase {

  use SignatureTrait, ValuesTrait;

  /**
   * A mock Subscription object.
   *
   * @var \Drupal\entity_share_websub_hub\SubscriptionInterface
   */
  protected $subscriptionMock;

  /**
   * The Queue factory service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * A Hub.
   *
   * @var \Drupal\entity_share_websub_hub\Hub
   */
  protected $hub;

  /**
   * A Queue object.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * Mock queue value.
   *
   * @var object
   */
  protected $queueValue;

  /**
   * A request object.
   *
   * @var \GuzzleHttp\Psr7\Request
   */
  public $currentRequest;

  /**
   * Guzzle http client with mock handler.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClientMock;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerChannelMock;

  /**
   * Mock up an http client.
   */
  protected function initHttpClientMock(): void {
    // Create a mock and queue two responses.
    $mock = new MockHandler([
      function (Request $request) {
        $this->currentRequest = $request;
        switch ($request->getMethod()) {
          case 'PUT':
            return new Response(202);

          case 'GET':
            $query = $request->getUri()->getQuery();
            parse_str($query, $request_params);
            return new Response(200, [], $request_params['hub_challenge']);
        }
        return new Response(400, [], 'This method is not allowed');
      },
      new Response(403, [], 'There was no such subscription request'),
    ]);

    $handlerStack = HandlerStack::create($mock);
    $this->httpClientMock = new Client(['handler' => $handlerStack]);
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->setUpValues();
    $this->subscriptionMock = $this->createMock(SubscriptionInterface::class);
    $this->subscriptionMock->method('getSubscribersBySids')
      ->willReturn([$this->mockSubscriber]);
    $this->subscriptionMock->method('getUpdates')
      ->willReturn([
        $this->mockSubscriber => [$this->mockTopic, $this->mockSecret],
      ]);
    $this->initHttpClientMock();

    $queue = $this->createMock(QueueInterface::class);
    $queue->method('claimItem')
      ->willReturnCallback(function () {
        return empty($this->queueValue) ? FALSE : $this->queueValue;
      });
    $queue->method('createItem')
      ->willReturnCallback(function ($arg) {
        $this->queueValue = (object) ['data' => $arg];
      });

    $queue->method('deleteItem')
      ->willReturnCallback(function () {
        unset($this->queueValue);
      });

    $queue->method('numberOfItems')
      ->willReturnCallback(function () {
        return empty($this->queueValue) ? 0 : 1;
      });

    $this->queueFactory = $this->createMock(QueueFactory::class);
    $this->queueFactory->method('get')
      ->willReturn($queue);
    $this->queue = $this->queueFactory->get(Hub::QUEUE_NAME);
    $this->loggerChannelMock = $this->createMock(LoggerChannelFactoryInterface::class);
    $this->loggerChannelMock->method('get')
      ->willReturn($this->createMock(LoggerChannelInterface::class));
    $this->hub = new Hub(
      $this->subscriptionMock,
      $this->queueFactory,
      $this->httpClientMock,
      $this->loggerChannelMock
    );
  }

  /**
   * Test notifications.
   */
  public function testNotify() {
    $this->hub->notify([2]);
    $item = $this->queue->claimItem();
    $this->assertEquals(['path' => $this->mockSubscriber], $item->data, 'Testing queue placement');

    $this->hub->processUpdates();
    $signature_header = $this->currentRequest->getHeader('X-Hub-Signature');
    $this->assertNotEmpty($signature_header, 'signature present');
    $this->assertEquals('sha256=3338c47a654fc7dc9e6c280e9247dac25f81b026df5b6e1ac76ed25c6acb5044', reset($signature_header), 'Test request signature correct');
    $this->assertEquals($this->currentRequest->getBody()
      ->getContents(), $this->mockTopic, 'Test request content');

  }

  /**
   * Test intent validator.
   */
  public function testValidateIntent() {
    $result = $this->hub->validateIntent('subscribe', $this->mockTopic, $this->mockSubscriber, $this->mockSecret);
    $this->assertEquals(TRUE, $result, 'Existing intent');
    $signature_header = $this->currentRequest->getHeader('X-Hub-Signature');
    $this->assertNotEmpty($signature_header, 'signature present');
    $query = $this->currentRequest->getUri()->getQuery();
    parse_str($query, $request_params);
    $this->assertEquals($this->getSignature([
      'hub.mode' => 'subscribe',
      'hub.topic' => $this->mockTopic,
      'hub.challenge' => $request_params['hub_challenge'],
      'hub.lease_seconds' => Hub::LEASE_SECONDS,
    ], $this->mockSecret), reset($signature_header), 'Test request signature correct');
    $result = $this->hub->validateIntent('subscribe', $this->mockTopic, $this->mockSubscriber, $this->mockSecret);
    $this->assertNotTrue($result, 'Non-existing intent');
  }

}
