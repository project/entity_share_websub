<?php

/**
 * @file
 * Provide a custom views field data that isn't tied to any other module.
 */

/**
 * Implements hook_views_data().
 */
function entity_share_websub_hub_views_data() {
  $table = 'entity_share_websub_hub_subscription';
  $data = [];
  $data[$table]['table']['group'] = t('Entity Share WebSub Hub');
  $data[$table]['table']['base'] = [
    'title' => t('Entity Share WebSub Hub Subscriptions'),
    'help' => t('Entity Share WebSub Hub Subscriptions data.'),
    'query_id' => 'views_query',
  ];
  $data[$table]['table']['join'] = [
    'users_field_data' => [
      'left_table' => 'entity_share_websub_hub_subscription',
      'left_field' => 'uid',
      'field' => 'uid',
    ],
  ];

  $data[$table]['entity_type'] = [
    'title' => t('Entity Type'),
    'help' => t('The field showing the type of syndicated content.'),
    'real field' => 'entity_type',
    'field' => [
      'id' => 'standard',
    ],
  ];

  $data[$table]['entity_id'] = [
    'title' => t('Entity ID'),
    'help' => t('The field showing the entity ID of a syndicated content'),
    'relationship' => [
      'title' => t('UUID'),
      'help' => t('UUID of content.'),
      'handler' => ['views_handler_relationship'],
      'id' => 'standard',
      'base' => 'node',
      'base field' => 'uuid',
      'field' => 'entity_id',
      'label' => t('uuid'),
    ],
    'real field' => 'entity_id',
    'field' => [
      'id' => 'standard',
    ],
  ];

  $data[$table]['syndicated_summary_link'] = [
    'title' => t('Syndicated Summary with link'),
    'help' => t('The field showing the summary of a syndicated content'),
    'real field' => 'content_summary',
    'field' => [
      'id' => 'syndicated_summary_link',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'title' => t('Summary'),
      'id' => 'summary_filter',
    ],
  ];

  $data[$table]['syndicated_by_field'] = [
    'title' => t('Syndicated by'),
    'help' => t('The field showing syndicators for a piece of content'),
    'real field' => 'subscriber_endpoint',
    'field' => [
      'id' => 'syndicated_by_field',
      'click sortable' => TRUE,
    ],
  ];
  $data[$table]['user_field'] = [
    'title' => t('Username'),
    'help' => t('The field showing username of the content creator'),
    'real field' => 'uid',
    'field' => [
      'id' => 'user_field',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'title' => t('Username'),
      'id' => 'user_filter',
    ],
  ];
  $data[$table]['syndicated_count_field'] = [
    'title' => t('Syndicated count field'),
    'help' => t('Shows the number of subscribers for a content.'),
    'field' => [
      'id' => 'syndicated_count_field',
      'click sortable' => TRUE,
    ],
  ];
  $data[$table]['syndication_status_field'] = [
    'title' => t('Syndication status'),
    'help' => t('Shows the status of the content subscription.'),
    'real field' => 'status',
    'field' => [
      'id' => 'syndication_status_field',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'title' => t('Entity Type'),
      'id' => 'entity_share_websub_syndicated',
    ],
  ];
  $data[$table]['syndicator_email_field'] = [
    'title' => t('Syndicator email'),
    'help' => t('Shows the email of the subscriber for the content.'),
    'real field' => 'user_email',
    'field' => [
      'id' => 'syndicator_email_field',
      'click sortable' => TRUE,
    ],
  ];

  $data[$table]['uid'] = [
    'title' => t('User ID'),
    'help' => t('Shows the user ID of the content creator.'),
    'real field' => 'uid',
    'field' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
      'help' => 'User ID',
    ],
  ];

  $data[$table]['subscription_id'] = [
    'title' => t('Subscription ID'),
    'help' => t('Shows the Subscription ID.'),
    'real field' => 'sid',
    'field' => [
      'id' => 'numeric',
      'click sortable' => TRUE,
    ],
    'argument' => [
      'id' => 'numeric',
      'help' => 'User ID',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  return $data;
}
