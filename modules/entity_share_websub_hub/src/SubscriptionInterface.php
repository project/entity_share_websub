<?php

namespace Drupal\entity_share_websub_hub;

use Drupal\Core\Entity\EntityInterface;

/**
 * Describes a service that maintains subscriptions.
 */
interface SubscriptionInterface {

  /**
   * Table name.
   */
  const TABLE_NAME = 'entity_share_websub_hub_subscription';

  const STATUS_ACTIVE = 1;
  const STATUS_CANCELLED = 0;

  const UPDATE_ON = 1;
  const UPDATE_OFF = 0;

  const VERIFIED = 1;
  const NOT_VERIFIED = 0;

  /**
   * Get existing subscriptions for an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return mixed
   *   Array of subscription ids.
   */
  public function checkSubscriptions(EntityInterface $entity);

  /**
   * Flags subscriptions for update.
   *
   * @param array $sids
   *   Subscription IDs.
   * @param array $meta
   *   Metadata to update for the subscription.
   * @param int $update
   *   Optional. Value of update flag (default 1).
   */
  public function updateSubscriptions(array $sids, array $meta, $update = 1);

  /**
   * Removes the update flag from subscriptions that have been processed.
   *
   * @param array $update
   *   Array of update records.
   */
  public function unflagSubscriptions(array $update);

  /**
   * Loads data for the notification of the subscriber.
   *
   * @param string $subscriber
   *   The subscriber endpoint.
   * @param bool $to_cancel
   *   Represents the desired notification type.
   *   FALSE means to prepare data for update.
   *   TRUE means to prepare data for cancelling.
   *
   * @return array
   *   The data.
   *
   * @SuppressWarnings(PHPMD.BooleanArgumentFlag)
   */
  public function getUpdates($subscriber = NULL, $to_cancel = FALSE);

  /**
   * Get subscribers by subscription IDs.
   *
   * @param array $sids
   *   Subscriber ids.
   *
   * @return mixed
   *   Result of the executed query.
   */
  public function getSubscribersBySids(array $sids);

  /**
   * Insert or update a subscription.
   *
   * @param string $topic
   *   Topic URL.
   * @param string $callback
   *   Callback URL.
   * @param string $secret
   *   Subscriber shared secret for this subscription.
   * @param string $email
   *   Initiator's email.
   * @param string $mode
   *   Mode: subscribe|unsubscribe.
   *
   * @return array
   *   Array: [int $http_code, string $message | int]
   */
  public function save($topic, $callback, $secret, $email, $mode);

  /**
   * Delete a subscription by condition.
   *
   * @param array $condition
   *   Array with keys: field, value and operator(optional).
   *
   * @throws \Exception
   */
  public function delete(array $condition);

  /**
   * Flags subscriptions as verified.
   *
   * @param string $callback
   *   Subscription callback.
   * @param int $verified
   *   Optional. Value of verified flag (default 1).
   */
  public function verifySubscription($callback, $verified = self::VERIFIED);

}
