<?php

namespace Drupal\entity_share_websub_hub\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Query\Merge;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Responds to subscription requests.
 */
class SubscriptionController extends ControllerBase {

  /**
   * Drupal\entity_share_websub_hub\SubscriptionInterface definition.
   *
   * @var \Drupal\entity_share_websub_hub\SubscriptionInterface
   */
  protected $subscription;

  /**
   * The hub service.
   *
   * @var \Drupal\entity_share_websub_hub\HubInterface
   */
  protected $hub;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->subscription = $container->get('entity_share_websub_hub.subscription');
    $instance->hub = $container->get('entity_share_websub_hub.hub');
    return $instance;
  }

  /**
   * Subscribe.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request on `/subscribe`.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response to the subscription request
   */
  public function subscribe(Request $request) {
    $topic = $request->get('hub_topic');
    $mode = $request->get('hub_mode');
    $callback = $request->get('hub_callback');
    $secret = $request->get('hub_secret');
    $email = $request->get('email');
    [$code, $result] = $this->subscription->save($topic, $callback, $secret, $email, $mode);
    if ($code == 202) {
      if ($result == Merge::STATUS_INSERT) {
        $validationResult = $this->hub->validateIntent($mode, $topic, $callback, $secret);
        if ($validationResult === TRUE) {
          $response = new Response('', $code);
          $this->subscription->verifySubscription($callback);
        }
        else {
          $response = new Response('We were not able to validate your subscription', 403);
          try {
            $this->subscription->delete([
              'field' => 'subscriber_endpoint',
              'value' => $callback,
            ]);
          }
          catch (\Exception $exception) {
            $response = new Response('We were not able to validate your subscription and had an issue processing this event', 503);
          }
        }
      }
      elseif ($result == Merge::STATUS_UPDATE) {
        $response = new Response('', $code);
      }

    }
    else {
      $response = new Response($result, $code);
    }
    return $response;
  }

}
