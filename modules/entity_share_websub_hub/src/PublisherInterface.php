<?php

namespace Drupal\entity_share_websub_hub;

use Drupal\Core\Entity\EntityInterface;

/**
 * Describes a service that informs the Hub of new content.
 */
interface PublisherInterface {

  /**
   * Notifies hub to update the channels relevant to the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   */
  public function notifyRelevant(EntityInterface $entity);

  /**
   * Breaks the syndication for the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   */
  public function disableSubscriptionsFor(EntityInterface $entity);

}
