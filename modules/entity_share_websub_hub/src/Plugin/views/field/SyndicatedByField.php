<?php

namespace Drupal\entity_share_websub_hub\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("syndicated_by_field")
 */
class SyndicatedByField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->field_alias = $this->query->addField(NULL, 'CONCAT(subscriber_endpoint, user_email)', 'websub_subscriptions');
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['hide_alter_empty'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $parsed = parse_url($values->websub_subscriptions);
    return $parsed['host'];
  }

}
