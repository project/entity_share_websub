<?php

namespace Drupal\entity_share_websub_hub\Plugin\views\field;

use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\entity_share_websub_hub\Subscription;
use Drupal\user\Entity\User;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 *
 * @phpstan-consistent-constructor
 *
 * @ViewsField("user_field")
 */
class UserField extends FieldPluginBase {

  /**
   * Entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $entity_repository) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityRepository = $entity_repository;
  }

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->field_alias = $this->query->addField(Subscription::TABLE_NAME, 'uid', 'user');
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['hide_alter_empty'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    try {
      $entity = User::load($values->user);
      if ($entity) {
        return $entity->toLink()->toString();
      }
    }
    catch (EntityMalformedException $exception) {
      return $exception->getMessage();
    }
    catch (EntityStorageException $exception) {
      return $exception->getMessage();
    }
    return $values->uid;
  }

}
