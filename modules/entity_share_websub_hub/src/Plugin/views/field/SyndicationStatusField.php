<?php

namespace Drupal\entity_share_websub_hub\Plugin\views\field;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\entity_share_websub_hub\Subscription;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("syndication_status_field")
 */
class SyndicationStatusField extends FieldPluginBase {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->field_alias = $this->query->addField(Subscription::TABLE_NAME, 'status', 'status');
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['hide_alter_empty'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    return $values->status ? $this->t('Active') : $this->t('Canceled');
  }

}
