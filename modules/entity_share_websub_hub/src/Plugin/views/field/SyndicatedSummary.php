<?php

namespace Drupal\entity_share_websub_hub\Plugin\views\field;

use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\entity_share_websub_hub\Subscription;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A handler to provide a content summary with an optional link.
 *
 * @ingroup views_field_handlers
 *
 * @phpstan-consistent-constructor
 *
 * @ViewsField("syndicated_summary_link")
 */
class SyndicatedSummary extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  final public function __construct(array $configuration, $plugin_id, $plugin_definition, protected EntityRepositoryInterface $entityRepository) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->field_alias = $this->query->addField(Subscription::TABLE_NAME, 'content_summary', 'content_summary');
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['hide_alter_empty'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    try {
      $entity = $this->entityRepository->loadEntityByUuid(
        $values->entity_share_websub_hub_subscription_entity_type,
        $values->entity_share_websub_hub_subscription_entity_id
      );
      if ($entity) {
        return $entity->toLink($values->content_summary)->toString();
      }
    }
    catch (EntityMalformedException $exception) {
      return $exception->getMessage();
    }
    catch (EntityStorageException $exception) {
      return $exception->getMessage();
    }
    return $values->content_summary;
  }

}
