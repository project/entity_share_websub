<?php

namespace Drupal\entity_share_websub_hub\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Drupal\views\Plugin\views\filter\Equality;

/**
 * Field handler to filter by content author.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsFilter("user_filter")
 */
class UserFilter extends Equality {

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    $allUsers = User::loadMultiple();
    $options = [];
    foreach ($allUsers as $user) {
      $options[$user->id()] = $user->label();
    }

    $form['value'] = [
      '#type' => 'select',
      '#title' => $this->t('Username'),
      '#description' => $this->t('Enter the username.'),
      '#options' => $options,
      '#default_value' => $this->value,
      '#process_default_value' => $this->isExposed(),
    ];
  }

}
