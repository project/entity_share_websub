<?php

namespace Drupal\entity_share_websub_hub\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\StringFilter;

/**
 * Field handler to filter by subscription summary.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsFilter("summary_filter")
 */
class SummaryFilter extends StringFilter {

}
