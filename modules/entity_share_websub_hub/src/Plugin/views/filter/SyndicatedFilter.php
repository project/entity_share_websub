<?php

namespace Drupal\entity_share_websub_hub\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\BooleanOperatorString;

/**
 * Field handler to filter syndication by status.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsFilter("entity_share_websub_syndicated")
 */
class SyndicatedFilter extends BooleanOperatorString {

}
