<?php

namespace Drupal\entity_share_websub_hub;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\entity_share_websub\SignatureTrait;
use GuzzleHttp\ClientInterface;

/**
 * Implements the Hub behaviors.
 */
class Hub implements HubInterface {
  use SignatureTrait;

  /**
   * Subscription service.
   *
   * @var \Drupal\entity_share_websub_hub\SubscriptionInterface
   */
  protected $subscription;

  /**
   * The hub notification queue.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * Injected Http Client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Injected Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerChannelFactory;

  /**
   * Process timeout value.
   *
   * @var int
   */
  private $processTimeout = 30;


  const QUEUE_NAME = 'entity_share_websub';

  const PAUSE_DURATION = 3;

  const LEASE_SECONDS = 60 * 60 * 24 * 14;

  const ACTION_CANCEL = 'cancel';

  /**
   * Constructs a new Hub object.
   */
  public function __construct(
    SubscriptionInterface $subscription,
    QueueFactory $queue,
    ClientInterface $http_client,
    LoggerChannelFactoryInterface $logger_channel_factory,
  ) {
    $this->subscription = $subscription;
    $this->queue = $queue->get(self::QUEUE_NAME);
    $this->queue->createQueue();
    $this->processTimeout = (ini_get('max_execution_time') ?: 30) - 5;
    $this->httpClient = $http_client;
    $this->loggerChannelFactory = $logger_channel_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function notifySubscriber($subscriber) {
    $this->queue->createItem([
      'path' => $subscriber,
    ]);
  }

  /**
   * Deferred subscriber notification.
   *
   * @param string $subscriber
   *   The subscriber url.
   */
  public function cancelSubscriber($subscriber) {
    $this->queue->createItem([
      'path' => $subscriber,
      'action' => self::ACTION_CANCEL,
    ]);
  }

  /**
   * Notify multiple subscriptions by id.
   *
   * @param array $sids
   *   Subscription ids.
   */
  public function notify(array $sids) {
    $subscribers = $this->subscription->getSubscribersBySids($sids);
    foreach ($subscribers as $subscriber) {
      $this->notifySubscriber($subscriber);
    }
  }

  /**
   * Cancel multiple subscriptions by id.
   *
   * @param array $sids
   *   Subscription ids.
   */
  public function cancel(array $sids) {
    $subscribers = $this->subscription->getSubscribersBySids($sids);
    foreach ($subscribers as $subscriber) {
      $this->cancelSubscriber($subscriber);
    }
  }

  /**
   * Process the updates in the queue.
   */
  public function processUpdates() {
    if ($this->queue->numberOfItems() > 0) {
      $end = time() + $this->processTimeout;
      $logger = $this->loggerChannelFactory->get('entity_share_websub_hub');
      while (time() < $end && ($item = $this->queue->claimItem())) {
        try {
          $subscriber = $item->data['path'];
          $to_cancel = !empty($item->data['action']) && $item->data['action'] == self::ACTION_CANCEL;
          $update = $this->subscription->getUpdates($subscriber, $to_cancel);
          if (!empty($update)) {
            if ($to_cancel) {
              $this->doCancel($update);
            }
            else {
              $this->doNotify($update);
            }
            $this->subscription->unflagSubscriptions($update);
          }
          $this->queue->deleteItem($item);
          sleep(self::PAUSE_DURATION);
        }
        catch (SuspendQueueException $e) {
          $this->queue->releaseItem($item);
          $logger->error($e->getMessage());
          break;
        }
        catch (\Exception $e) {
          $logger->error($e->getMessage());
          $this->queue->deleteItem($item);
        }

      }

    }

  }

  /**
   * {@inheritdoc}
   */
  public function doNotify(array $update) {
    foreach ($update as $subscriber => $data) {
      [$payload, $secret] = $data;
      $this->httpClient->put($subscriber, [
        'body' => $payload,
        'headers' => [
          'X-Hub-Signature' => $this->getSignature($payload, $secret),
        ],
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function doCancel($subject) {
    foreach ($subject as $subscriber => $data) {
      [$payload, $secret] = $data;
      $this->httpClient->delete($subscriber, [
        'query' => [
          'topic' => $payload,
          'timestamp' => time(),
        ],
        'headers' => [
          'X-Hub-Signature' => $this->getSignature($payload, $secret),
        ],
      ]);
    }
  }

  /**
   * Validates subscriber's intent.
   *
   * @param string $mode
   *   The request mode.
   * @param string $topic
   *   The topic id string.
   * @param string $callback
   *   The subscription url.
   * @param string $secret
   *   The subscription secret.
   *
   * @return bool|string
   *   Returns the validation state or an error message.
   */
  public function validateIntent($mode, $topic, $callback, $secret) {
    $challenge = uniqid('websub-', TRUE);
    $params = [
      'hub.mode' => $mode,
      'hub.topic' => $topic,
      'hub.challenge' => $challenge,
      'hub.lease_seconds' => self::LEASE_SECONDS,
    ];
    try {
      $response = $this->httpClient->get($callback, [
        'query' => $params,
        'headers' => [
          'X-Hub-Signature' => $this->getSignature($params, $secret),
        ],
      ]);
      if ($response->getBody()->getContents() == $challenge) {
        return TRUE;
      }
      return FALSE;
    }
    catch (\Exception $exception) {
      return $exception->getMessage();
    }
  }

}
