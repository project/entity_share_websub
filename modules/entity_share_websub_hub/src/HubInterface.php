<?php

namespace Drupal\entity_share_websub_hub;

/**
 * Models the required behaviors of a Hub.
 */
interface HubInterface {

  /**
   * Process the updates in the queue.
   */
  public function processUpdates();

  /**
   * Deferred subscriber notification.
   *
   * @param string $subscriber
   *   The subscriber.
   */
  public function notifySubscriber($subscriber);

  /**
   * Executes the notify requests.
   *
   * @param array $update
   *   Array: [$subscriber => [$payload, $secret]].
   */
  public function doNotify(array $update);

  /**
   * Executes the cancellation request.
   *
   * @param array $subject
   *   Array: [$subscriber => [$payload, $secret]].
   */
  public function doCancel(array $subject);

}
