<?php

namespace Drupal\entity_share_websub_hub\EventSubscriber;

use Drupal\entity_share_websub_hub\HubInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\TerminateEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Processes notification to subscribers.
 */
class NotificationProcessor implements EventSubscriberInterface {

  /**
   * Drupal\entity_share_websub_hub\HubInterface definition.
   *
   * @var \Drupal\entity_share_websub_hub\HubInterface
   */
  protected $hub;

  /**
   * Constructs a new NotificationProcessor object.
   */
  public function __construct(HubInterface $entity_share_websub_hub_hub) {
    $this->hub = $entity_share_websub_hub_hub;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];
    $events[KernelEvents::TERMINATE] = ['kernelTerminate'];
    return $events;
  }

  /**
   * This method is called when the kernel.terminate is dispatched.
   *
   * @param \Symfony\Component\HttpKernel\Event\TerminateEvent $event
   *   The dispatched event.
   */
  public function kernelTerminate(TerminateEvent $event) {
    $this->hub->processUpdates();
  }

}
