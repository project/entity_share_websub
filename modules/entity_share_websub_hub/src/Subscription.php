<?php

namespace Drupal\entity_share_websub_hub;

use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\Entity\Node;

/**
 * Manages interaction with the Subscriber.
 */
class Subscription implements SubscriptionInterface {

  /**
   * Drupal\Core\Database\Connection definition.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Email validator service.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;


  /**
   * Entity Type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * Constructs a new Subscription object.
   */
  public function __construct(
    Connection $database,
    EmailValidatorInterface $email_validator,
    EntityTypeManagerInterface $entity_type_manager,
    EntityRepositoryInterface $entity_repository,
  ) {
    $this->database = $database;
    $this->emailValidator = $email_validator;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityRepository = $entity_repository;
  }

  /**
   * {@inheritdoc}
   */
  public function checkSubscriptions(EntityInterface $entity) {
    $query = $this->database->select(self::TABLE_NAME, 'e');
    $query->condition('entity_type', $entity->getEntityTypeId());
    $query->condition('entity_id', $entity->uuid());
    $query->condition('status', self::STATUS_ACTIVE);
    $query->condition('is_verified', self::VERIFIED);
    $query->fields('e', ['sid']);
    return $query->execute()->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function updateSubscriptions(array $sids, array $meta = [], $update = 1) {
    $updates = array_filter($meta, function ($value) {
      return !is_null($value);
    });
    $updates['update_flag'] = $update;
    $query = $this->database->update(self::TABLE_NAME);
    $query->condition('sid', $sids, 'IN');
    $query->fields($updates);
    $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function unflagSubscriptions(array $update) {
    $subscribers = array_keys($update);
    $this->database->update(self::TABLE_NAME)
      ->condition('subscriber_endpoint', $subscribers, 'IN')
      ->fields(['update_flag' => self::UPDATE_OFF])
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function verifySubscription($callback, $verified = self::VERIFIED) {
    $query = $this->database->update(self::TABLE_NAME);
    $query->condition('subscriber_endpoint', $callback);
    $query->fields(['is_verified' => $verified]);
    $query->execute();
  }

  /**
   * {@inheritdoc}
   *
   * @SuppressWarnings(PHPMD.BooleanArgumentFlag)
   */
  public function getUpdates($subscriber = NULL, $to_cancel = FALSE) {
    $query = $this->database->select(self::TABLE_NAME, 'e');
    $query->fields(
      'e',
      [
        'subscriber_endpoint',
        'entity_type',
        'entity_id',
        'subscriber_secret',
        'channel_id',
      ]
    );
    $query->condition('update_flag', self::UPDATE_ON);
    $status = $to_cancel ? self::STATUS_CANCELLED : self::STATUS_ACTIVE;
    $query->condition('status', $status);
    $query->condition('is_verified', self::VERIFIED);
    if (!empty($subscriber)) {
      $query->condition('subscriber_endpoint', $subscriber);
    }

    $rows = $query->execute()->fetchAllAssoc('subscriber_endpoint');

    $result = [];
    foreach ($rows as $row) {
      $result[$row->subscriber_endpoint] = [
        "{$row->channel_id}/{$row->entity_id}",
        $row->subscriber_secret,
      ];
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getSubscribersBySids(array $sids) {
    $query = $this->database->select(self::TABLE_NAME, 'e');
    $query->condition('sid', $sids, 'IN');
    $query->groupBy('subscriber_endpoint');
    $query->fields('e', ['subscriber_endpoint']);
    return $query->execute()->fetchCol();
  }

  /**
   * Insert or update a subscription.
   *
   * @param string $topic
   *   Topic URL.
   * @param string $callback
   *   Callback URL.
   * @param string $secret
   *   Subscriber shared secret for this subscription.
   * @param string $email
   *   Initiator's email.
   * @param string $mode
   *   Mode: subscribe|unsubscribe.
   *
   * @return array
   *   Array: [int $http_code, string $message | int]
   */
  public function save($topic, $callback, $secret, $email, $mode) {
    if (!preg_match('#^(\w+)/([\w\-]+)#', $topic, $parts)) {
      return [400, 'Incorrect topic format'];
    }
    [, $channel_id, $entity_id] = $parts;
    /** @var \Drupal\entity_share_server\Entity\ChannelInterface[] $channels */
    $channels = $this->entityTypeManager->getStorage('channel')->loadByProperties(['id' => $channel_id]);
    if (empty($channels)) {
      return [400, 'Provided channel was not found'];
    }
    $callback = UrlHelper::stripDangerousProtocols($callback);
    if (!UrlHelper::isValid($callback, TRUE)) {
      return [400, 'Provided callback is not a valid URL'];
    }
    $secret = Html::escape($secret);
    if (empty($secret)) {
      return [400, 'No secret provided'];
    }
    if (!preg_match('#(un)?subscribe#', $mode)) {
      return [400, 'Unrecognized mode'];
    }
    if (!$this->emailValidator->isValid($email)) {
      return [400, 'Provided email is not valid'];
    }
    $channel = reset($channels);
    $channel_entity_type = $channel->get('channel_entity_type');
    $entityType = $this->entityTypeManager->getStorage($channel_entity_type)->getEntityType();
    $table = $entityType->getBaseTable();
    $existing_query = $this->database->select($table, 't');
    $existing_query->condition('uuid', $entity_id);
    $existing_query->fields('t', [$entityType->getKeys()['id']]);
    $existing = $existing_query->execute()->fetchCol();
    if (empty($existing)) {
      return [404, 'No such topic to subscribe to'];
    }
    try {
      $node = Node::load(reset($existing));
      $result = [
        202,
        $this->database->merge(self::TABLE_NAME)->insertFields(
          [
            'subscriber_endpoint' => $callback,
            'entity_type' => $channel_entity_type,
            'entity_id' => $entity_id,
            'status' => $mode == 'subscribe' ? self::STATUS_ACTIVE : self::STATUS_CANCELLED,
            'update_flag' => self::UPDATE_OFF,
            'subscriber_secret' => $secret,
            'user_email' => $email,
            'content_summary' => $node->get('title')->value . ' : ' . $node->bundle(),
            'uid' => $node->get('uid')->target_id,
            'channel_id' => $channel_id,
          ])
          ->updateFields([
            'status' => $mode == 'subscribe' ? self::STATUS_ACTIVE : self::STATUS_CANCELLED,
          ])
          ->key('subscriber_endpoint', $callback)
          ->execute(),
      ];
      Cache::invalidateTags(['node_list']);
      return $result;
    }
    catch (\Exception $exception) {
      return [503, "Error saving subscription: {$exception->getMessage()}"];
    }
  }

  /**
   * Delete a subscription by condition.
   *
   * @param array $condition
   *   Array with keys: field, value and operator(optional).
   *
   * @throws \Exception
   */
  public function delete(array $condition) {
    if (empty($condition['field']) || empty($condition['value'])) {
      throw new \Exception('Invalid delete condition');
    }
    $field = $condition['field'];
    $value = $condition['value'];
    $operator = '=';
    if (!empty($condition['operator'])) {
      $operator = $condition['operator'];
    }
    try {
      $this->database->delete(self::TABLE_NAME)
        ->condition($field, $value, $operator)
        ->execute();
    }
    catch (\Exception $exception) {
      throw new \Exception("Subscription delete exception: {$exception->getMessage()}");
    }
  }

}
