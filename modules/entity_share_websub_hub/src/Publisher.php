<?php

namespace Drupal\entity_share_websub_hub;

use Drupal\Core\Entity\EntityInterface;

/**
 * Maintains the remote status of subscribed content.
 */
class Publisher implements PublisherInterface {

  /**
   * Injected Hub service.
   *
   * @var \Drupal\entity_share_websub_hub\HubInterface
   */
  protected $hub;

  /**
   * Injected subscription service.
   *
   * @var \Drupal\entity_share_websub_hub\Subscription
   */
  protected $subscription;

  /**
   * Constructs a new Publisher object.
   */
  public function __construct(HubInterface $hub, Subscription $subscription) {
    $this->hub = $hub;
    $this->subscription = $subscription;
  }

  /**
   * A service that informs the Hub of new content.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   */
  public function notifyRelevant(EntityInterface $entity) {
    $subscriptions = $this->subscription->checkSubscriptions($entity);
    if (!empty($subscriptions)) {
      $meta = [
        'content_summary' => $entity->get('title')->value . ' : ' . $entity->bundle() ?? NULL,
        'uid' => $entity->get('uid')->target_id ?? NULL,
      ];
      $this->subscription->updateSubscriptions($subscriptions, $meta);
      $this->hub->notify($subscriptions);
    }
  }

  /**
   * Breaks the syndication for the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   */
  public function disableSubscriptionsFor(EntityInterface $entity) {
    $subscriptions = $this->subscription->checkSubscriptions($entity);
    if (!empty($subscriptions)) {
      $this->subscription->updateSubscriptions($subscriptions, ['status' => Subscription::STATUS_CANCELLED]);
      $this->hub->cancel($subscriptions);
    }
  }

}
