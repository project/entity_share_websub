<?php

/**
 * @file
 * Contains entity_share_websub_hub.module.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\node\NodeInterface;

/**
 * Implements hook_entity_update().
 */
function entity_share_websub_hub_entity_update(EntityInterface $entity) {
  /** @var \Drupal\entity_share_websub_hub\PublisherInterface $publisher */
  if ($entity instanceof FieldableEntityInterface) {
    $publisher = \Drupal::service('entity_share_websub_hub.publisher');

    // Notifications for nodes.
    if ($entity instanceof NodeInterface) {
      if ($entity->isDefaultRevision()) {
        if ($entity->isPublished()) {
          $publisher->notifyRelevant($entity);
        }
        else {
          $publisher->disableSubscriptionsFor($entity);
        }
      }
      return;
    }

    // Notifications for other types of entities.
    if (!$entity->hasField('status') || $entity->get('status')->getString() == 1) {
      $publisher->notifyRelevant($entity);
    }
    else {
      $publisher->disableSubscriptionsFor($entity);
    }
  }
}

/**
 * Implements hook_entity_delete().
 */
function entity_share_websub_hub_entity_delete(EntityInterface $entity) {
  /** @var \Drupal\entity_share_websub_hub\PublisherInterface $publisher */
  $publisher = \Drupal::service('entity_share_websub_hub.publisher');
  $publisher->disableSubscriptionsFor($entity);
}
