<?php

namespace Drupal\entity_share_websub;

use Drupal\Component\Utility\UrlHelper;

/**
 * Provides a reusable method for hashing signatures.
 */
trait SignatureTrait {

  /**
   * Get the hub signature for websub protocol.
   *
   * @param array|object $data
   *   A serializable data structure.
   * @param string $secret
   *   A secret string.
   *
   * @return string
   *   The signature.
   */
  protected function getSignature($data, $secret) {
    if (is_array($data)) {
      parse_str(UrlHelper::buildQuery($data), $data);
    }
    $signature = hash('sha256', $secret . serialize($data));
    return "sha256=$signature";
  }

}
