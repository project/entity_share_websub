# Entity Share WebSub
This module extends the Entity Share module with continuous, automatic, content
updates.  The subscription and update process is inspired by the WebSub
protocol.

## Configuration

Entity Share defines various configurations for import at
`/admin/config/services/entity_share/import_config`.
This module is configured at `/admin/config/entity-share-websub-subscriber`
which requires the selection of
one of these sets of import config for use in our automated process.

In addition, there are these options:

* Hide the button provided by Entity Share that triggers immediate entity sync
  as that is handled by our automation.
* Ask the user about canceling the subscription after editing of imported
  content, along with a customizable message.
